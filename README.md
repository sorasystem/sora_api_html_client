# Repo for SORA Blocks #

This has the index.html which gets the json data from the Rails API at sorablocks.herokuapp.com
It also has some code from tutorials


### Todo ###

* Make a blockchain system using Ethereum


### How to install Ethereum ###

npm install -g ethereumjs-testrpc

npm init on root folder

npm install ethereum/web3.js --save

ethereum remix address must be http://remix.eth.. not https://

create index.html
  - make script that integrates web3.js 
  - copy the ABI from the Remix IDE into the web3.js

How to kill testrpc: fuser -k -n tcp 8545
